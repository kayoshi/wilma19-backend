'use strict';

/**
 * wilma-19-willkommen service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wilma-19-willkommen.wilma-19-willkommen');
