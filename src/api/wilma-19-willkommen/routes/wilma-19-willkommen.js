'use strict';

/**
 * wilma-19-willkommen router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wilma-19-willkommen.wilma-19-willkommen');
