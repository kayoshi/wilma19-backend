'use strict';

/**
 * wilma-19-post router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wilma-19-post.wilma-19-post');
