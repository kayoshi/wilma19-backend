'use strict';

/**
 * plattenkosmos-post controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::plattenkosmos-post.plattenkosmos-post');
