'use strict';

/**
 * plattenkosmos-post router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::plattenkosmos-post.plattenkosmos-post');
